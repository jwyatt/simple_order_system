# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

fake_data = true

if User.no_users_exist?
  u = User.create(name: "jwyatt", email: ENV['MASTER_EMAIL'], password: ENV['MASTER_PW'], password_confirmation: ENV['MASTER_PW'])
  u.save!

  u = User.create(name: "fake_user", email: "fake_user@example.com", password: ENV['MASTER_PW'], password_confirmation: ENV['MASTER_PW'])
  u.save!

  p "User generated #{u.name}, #{u}"
end

25.times do
  Order.new(name: Faker::Name.name, email: Faker::Internet.email, city: Faker::Address.city, address: Faker::Address.street_address, order_datetime: Faker::Date.backward(30)
  ).save!
end
p "Order generated"
