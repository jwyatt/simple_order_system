class CreateOrders < ActiveRecord::Migration[4.2]
  def change
    create_table :orders do |t|
      t.string :name
      t.string :email
      t.integer :order_amt
      t.string :price # numbers are strings because I don't want to do currency
      t.string :shipping_amt
      t.string :tax
      t.string :final_price
      t.datetime :order_datetime
      t.string :desc
      t.string :shipping_code # usps/ups tracking code
      t.integer :shipping_type # usps or ups? 1, 2
      t.string :address
      t.string :address2
      t.string :city
      t.string :zipcode
      t.string :shipping_notes
      t.string :return_notes
      t.integer :shipper_id
      t.integer :returner_id
      t.integer :cancelled_id
      t.string :country

      t.integer :order_status
      t.datetime :order_datetime
      t.datetime :shipped_datetime
      t.datetime :return_datetime
      t.boolean :shipped, default: false
      t.boolean :returned, default: false
      t.boolean :cancelled, default: false

      t.timestamps null: false
    end
  end
end
