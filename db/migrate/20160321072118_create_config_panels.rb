class CreateConfigPanels < ActiveRecord::Migration[4.2]
  def change
    create_table :config_panels do |t|
      t.float :state_tax_rate
      t.string :usps_id
      t.string :usps_pw
      t.string :ups_token
      t.string :name
      t.string :address
      t.string :address2
      t.string :city
      t.string :state
      t.string :zipcode
      t.string :shipping_country

      t.timestamps null: false
    end
  end
end
