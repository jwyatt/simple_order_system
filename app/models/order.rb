class Order < ActiveRecord::Base
  def self.all_new
    return Order.all.where(shipped: false, returned: false, cancelled: false ).sort_by &:order_datetime
  end

  def self.all_shipped_returned
    return Order.all.where(shipped: true )
  end

end
