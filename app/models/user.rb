class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  before_save :is_first_user?

  def self.no_users_exist?
    return true if User.all.count == 0
    return false
  end

  def is_first_user?
    self.acct_type = 1 if User.all.count == 0
    ConfigPanel.get_cp
  end

  def is_admin?
    return true if self.acct_type == 1
    return false
  end

end
