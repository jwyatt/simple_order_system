class ConfigPanel < ActiveRecord::Base
  def self.get_cp
    if ConfigPanel.all.count > 0
      return ConfigPanel.first
    else
      cp = ConfigPanel.new
      cp.save!
      return cp
    end
  end

  def has_usps?
    return false
  end

  def has_ups?
    return false
  end
end
