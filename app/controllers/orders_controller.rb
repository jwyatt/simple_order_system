class OrdersController < ApplicationController
  before_action :check_permissions
  before_action :set_order, only: [:show, :edit, :update, :destroy, :shipped, :returned, :cancelled]
  before_action :demo_check, only: [:create, :update, :destroy, :shipped, :returned, :cancelled]

  def shipped
    @order.shipped = true
    @order.shipper_id = current_user.id
    @order.shipped_datetime = DateTime.now
    @order.save!
    redirect_to orders_path
  end

  def returned
    @order.returned = true
    @order.returner_id = current_user.id
    @order.return_datetime = DateTime.now
    @order.save!
    redirect_to orders_path
  end

  def cancelled
    @order.cancelled = true
    @order.cancelled_id = current_user.id
    @order.save!
    redirect_to orders_path
  end

  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.all
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
  end

  # GET /orders/new
  def new
    @order = Order.new
  end

  # GET /orders/1/edit
  def edit
  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(order_params)
    @order.order_datetime = DateTime.now

    respond_to do |format|
      if @order.save
        format.html { redirect_to @order, notice: 'Order was successfully created.' }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    return redirect_to root_path
    #@order.destroy
    #respond_to do |format|
    #  format.html { redirect_to orders_url, notice: 'Order was successfully destroyed.' }
    #  format.json { head :no_content }
    #end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:name, :order_amt, :price, :shipping_amt, :tax, :final_price, :order_datetime, :desc, :shipping_code, :shipping_type, :address, :address2, :city, :zipcode, :shipping_notes, :return_notes, :country)
    end

    def check_permissions
      return redirect_to root_path, notice: "Please log in" if not current_user
    end

    def demo_check
      redirect_to orders_path, notice: "Not allowed in Demo Mode" if ENV['DEMO_MODE']
    end
end
