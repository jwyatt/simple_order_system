class PagesController < ApplicationController
  def home
    if ENV['DEMO_MODE']
      @orders = Order.all
    end

    return redirect_to orders_path if current_user
  end

  def controlpanel
    return redirect_to root_path, notice: "Please log in" if not current_user

    return redirect_to root_path, notice: "Only Admin users can access" if not current_user.is_admin?

  end

  def shipping_calculator
    return redirect_to root_path, notice: "Please log in" if not current_user
  end
end
