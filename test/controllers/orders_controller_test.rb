require 'test_helper'

class OrdersControllerTest < ActionController::TestCase
  setup do
    @order = orders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:orders)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create order" do
    assert_difference('Order.count') do
      post :create, order: { address2: @order.address2, address: @order.address, city: @order.city, desc: @order.desc, final_price: @order.final_price, name: @order.name, order_amt: @order.order_amt, order_datetime: @order.order_datetime, price: @order.price, return_datetime: @order.return_datetime, return_notes: @order.return_notes, returner_id: @order.returner_id, shipper_id: @order.shipper_id, shipping_amt: @order.shipping_amt, shipping_code: @order.shipping_code, shipping_notes: @order.shipping_notes, shipping_type: @order.shipping_type, tax: @order.tax, zipcode: @order.zipcode }
    end

    assert_redirected_to order_path(assigns(:order))
  end

  test "should show order" do
    get :show, id: @order
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @order
    assert_response :success
  end

  test "should update order" do
    patch :update, id: @order, order: { address2: @order.address2, address: @order.address, city: @order.city, desc: @order.desc, final_price: @order.final_price, name: @order.name, order_amt: @order.order_amt, order_datetime: @order.order_datetime, price: @order.price, return_datetime: @order.return_datetime, return_notes: @order.return_notes, returner_id: @order.returner_id, shipper_id: @order.shipper_id, shipping_amt: @order.shipping_amt, shipping_code: @order.shipping_code, shipping_notes: @order.shipping_notes, shipping_type: @order.shipping_type, tax: @order.tax, zipcode: @order.zipcode }
    assert_redirected_to order_path(assigns(:order))
  end

  test "should destroy order" do
    assert_difference('Order.count', -1) do
      delete :destroy, id: @order
    end

    assert_redirected_to orders_path
  end
end
